# Bluetooth-Scanner für Android

## Allgemeines ##

Diese App dient zum Scannen nach Bluetooth-Geräten. Dabei kann nach verschiedenen Eigenschaften der Geräte gescannt werden. Dazu gehören der Gerätename (sofern vergeben) natürlich die MAC-Adresse. Wahlweise können auch beide Eigenschaften gleichzeitig ausgelesen werden. Neben den Bluetooth-Eigenschaften der Geräte wird auch die aktuelle GPS-Position des Scanners ermittelt, wenn GPS aktiviert ist. Dies dient zum Anzeigen des ungefähren Standortes der erkannten Geräte.

Die Anzeige der erkannten Geräte erfolgt als Liste in der Hauptansicht. Die Unterscheidung zwischen neuen Geräten (vorher noch nicht erkannt) und bereits bekannten Geräten erfolgt anhand einer Datenbank, in die jedes Gerät bei der ersten Erkennung eingetragen wird. In der Liste werden neue und bekannte Geräte farblich voneinander getrennt.

Zusätzlich kann anhand der MAC-Adresse der Hersteller des Bluetooth-Chips ermittelt werden. Die gelingt aber nur, wenn der Hersteller sich an die Konventionen hält und in den ersten drei Blöcken der MAC sein Unternehmen codiert. Anhand dieser Funktion wird im Programm unterschieden, ob es sich um ein Telefon oder ein anderes Gerät (Navigationsgerät, Headset, ...) handelt. Diese Information dient der späteren Auswertung.

Weiterhin kann die (eigene) GPS-Geschwindigkeit bei jedem erkannten Gerät mit angezeigt und in die Log-Datei geschrieben werden.

Einzelne Geräte können per Kontextmenü in eine Ignorierliste aufgenommen werden, so dass sie beim Scannen zwar erkannt, aber nicht in die Logdatei geschrieben bzw. auf dem Display angezeigt werden.

Daneben können Log-Dateien eingelesen und die darin enthaltenen Geräte in GoogleMaps angezeigt werden. Einzelne Geräte können über das Kontextmenü in der Listenansicht in GoogleMaps dargestellt werden.

---
## Voraussetzungen ##

In Android Studio muss das Modul google-play-service eingebunden (sein in Eclipse als Library). Sonst kommt es beim Compilieren und Ausführen der App auf dem Smartphone zu Fehlermeldungen.

#### für Android Studio: ####

 - File - Project Structure
 - Modules - Import Module - androidSDK/extras/google/google-play-service

Zusätzlich sind folgende Libraries notwendig:

 - android-support-v4.jar
 - guava-14.0.1.jar

Diese müssen über File - Project Structure eingebunden werden.

Für die Nutzung der GoogleMaps-Funktion ist ein Android Key für die Google Maps Android API v2 notwendig. Den kann man sich in der [Google Cloud Console](https://cloud.google.com/console#/project) erstellen.

Der so generierte Key muss in die AndroidManifest.xml innerhalb des Application-Tags eingefügt werden:

    ...
    <application
        android:allowBackup="true"
        android:icon="@drawable/appicon"
        android:label="@string/app_name"
        android:theme="@style/AppTheme" >
        <meta-data
            android:name="com.google.android.maps.v2.API_KEY"
            android:value="hier_muss_der_Key_rein"/>
        <activity
            android:name="com.example.app.MainActivity"
            android:label="@string/app_name"
            android:launchMode="singleTop">
    ...

---

## Todo

*siehe Tickets*

---

## Änderungen

***Version 5.2.4***

`ADDED`
  >+ Auslesen und Mitloggen der GPS-Geschwindigkeit
  >+ Mitschreiben des ausgwählten Strassentyps in der Logdatei. Auswahl des Strassentyps über Dropdownliste
  
`FIXED`
  >+ Force Close beim Aufruf von Google Maps repariert (Fehler in der Modulabhängigkeit im Project)
  >+ Force Close beim Aufruf mancher Optionen behoben

`UPDADED`
  >+ Liste mit den Herstellern aktualisiert (12.05.2014) (19328 Einträge)
  >+ Liste mit den Herstellern aktualisiert (16.06.2014) (19456 Einträge)
  >+ Sonim und Huawei in die Liste der Telefone eingefügt
  >+ Sony Mobile in die Liste der Telefone eingefügt
  >+ zte corporation in die Liste der Telefone eingefügt
  >+ TCT Mobile Limited in die Liste der Telefone eingefügt

***Version 5.2.3***

`ADDED`
  >+ Funktion zum Ermitteln, ob es sich um ein Telefon handelt, oder um ein anderes Bluetooth-Gerät
  >+ Funktion zum Aktivieren/Deaktivieren des Mitloggens der ID (wenn die ID nicht mit in die Log-Datei geschrieben wird, lässt sich das leichter in eine MySQL-Datenbank importieren)

`FIXED`
  >+ fehlerhaftes Einlesen der Logdatei in Google Maps, wenn die ID-Spalte fehlt

`UPDADED`
  >+ Ufine in die Liste der Telefone eingefügt (Hersteller des Bluetooth-Chips für z.B. Jiayu-China-Smartphones)
  >+ Apple in die Liste der Telefone eingefügt
  >+ Liste mit den Herstellern aktualisiert (24.02.2014) (19048 Einträge)
  >+ Liste mit den Herstellern aktualisiert (09.03.2014) (19098 Einträge)

***Version 5.2.2***

`ADDED`
  >+ Markierung (Text) für den Status der Ignorierliste (an/aus) in der Hauptansicht
  >+ Markierung (Text) für den Loggingstatus (an/aus) in der Hauptansicht
  >+ farbliche Markeirung für Loggingstatus und Status der Ignorierliste (rot = deaktiviert, grün = aktiviert)
  >+ Option zum Umschalten der Darstellung der Geräteinformationen zwischen zeilenweise und fortlaufend

`FIXED`
  >+ ForceClose beim Hinzufügen eines Gerätes zur Ignorierliste
  >+ Buttonbreite und Layout
  >+ ignorierte Geräte erhöhen nicht mehr den Gerätezähler (fortlaufende Nummer)

`UPDADED`
  >+ Liste mit den Herstellern aktualisiert (07.01.2014)
  >+ Liste mit den Herstellern aktualisiert (29.01.2014)
  >+ Liste mit den Herstellern aktualisiert (12.02.2014)

***Version 5.2.1***

`ADDED`
  >+ Funktion zum Ausschliessen von Geräten per Kontextmenü Ignorierliste
  >+ Aktivieren/Deaktivieren der Ignorierenfunktion in den Optionen
  >+ Entfernen der Geräte aus der Ignorierliste per Kontextmenü

`FIXED`
  >+ Einlesen der Logdatei und Anzeigen der Geräte in GoogleMaps
  >+ kleinere Codeoptimierungen

***Version 5.2.0***

`ADDED`
  >+ Herstellerabfrage (Hersteller erscheint in ListView und kann mit geloggt werden, wenn in den Optionen aktiviert)

`FIXED`
  >+ Aufbau der Logdatei geändert. Diese kann jetzt in andere Programme importiert werden wie eine CSV-Datei

`UPDADED`
  >+ Liste mit den Herstellern aktualisiert

***Version 5.1.4***

`ADDED`
  >+ Möglichkeit, das Display während des Scannens aktiv zu lassen
  >+ neues Logo

`FIXED`
  >+ Kontextmenü in der Listenansicht des Scanners zum Auslesen der Herstellers des Bluetooth-Chips und zum Anzeigen des gewählten Eintrages in GoogleMaps
  >+ mögliche NullpointerExceptions entfernt (Code optimiert)
    
`REMOVED`
  >+ KML-Export entfernt, da nicht funktionsfähig. Evtl. wird das später noch einmal eingefügt.
    
***Version 5.1.0***

`ADDED`
  >+ KML-Export
    
`FIXED`
  >+ mehrfach auftretende Geräte in den Logfiles werden nur einmal in GoogleMaps eingetragen

***Version 5.0.0***

`ADDED`
  >+ Implementierung von GoogleMaps
  >+ Einlesen des Logfiles und Eintragen der gültigen Datensätze in GoogleMaps

`FIXED`
  >+ Schieberegler der Vibrationseinstellungen erhält und setzt nun die richtigen Werte
  >+ mögliche NullpointerExceptions entfernt (Code optimiert)
  
***Version 4.0.0***
  >+ Implementierung von Funktion zur Unterscheidung neuer und bekannter Geräte (Farbein in der ListView, unterschiedliche Vibrationsdauern)
  
 ***Version 3.0.0***
 
  >+ Änderung der der Oberfläche (klickbare ListView statt TextView) 

***Version 2.0.0***

  >+ Unterscheidung neur und bekanter Geräte mittels einer Datenbank
  >+ Implementierung des Optionsmenüs

***Version 1.0.0***

  >+ erste Version mit Grundfunktion zum Scannen