package cl.app.bluetoothlogger.utilities;

import java.util.Comparator;

/**
 * Klasse zum Sortieren von mehrdimensionalen Arrays
 * setSortColumn(int c) muss aufgerufen werden, um festzulegen, nach welcher Spalte sortiert werden soll
 */
public class StringArrayComparator implements Comparator
{
    private int sortColumn = 0;

    public int setSortColumn(int c)
    {
        sortColumn = c;
        return c;
    }

    public int compare(Object o1, Object o2)
    {
        if (o1 == null && o2 == null)
            return 0;
        if (o1 == null)
            return -1;
        if (o2 == null)
            return 1;
        String[] s1 = (String[])o1;
        String[] s2 = (String[])o2;
        return s1[sortColumn].compareTo(s2[sortColumn]);
    }
}

