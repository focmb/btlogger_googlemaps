package cl.app.bluetoothlogger.utilities;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import cl.app.bluetoothlogger.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Klasse zum Auslesen der Herstellers, dem die MAC-Adresse zugewiesen wurde
 */
public class getVendorInformation
{
    private final ArrayList<String> vendors = new ArrayList<String>();
    private final String[] phoneNames = {
                                            "Nokia",
                                            "Motorola",
                                            "Samsung",
                                            "Rim",
                                            "Research in Motion",
                                            "LG",
                                            "Sony Ericsson",
                                            "Sony Mobile",
                                            "HTC Corporation",
                                            "Apple",
                                            "Ufine",
                                            "Sonim",
                                            "Huawei",
                                            "zte corporation",
                                            "TCT Mobile Limited"
    };

    /**
     * Einlesen der Herstellerliste aus der Textdatei im Assetsordner
     * @param c Context, aus dem die Funktion aufgerufen wird
     */
    public getVendorInformation(Context c)
    {
        AssetManager am = c.getAssets();
        try
        {
            InputStream in = am.open("allVendors.txt");
            BufferedReader buf = new BufferedReader(new InputStreamReader(in));
            String line;
            while((line = buf.readLine()) != null)
            {
                vendors.add(line);
            }
            buf.close();
            in.close();
        }
        catch(IOException ex)
        {
            Log.e(c.getString(R.string.app_name), ex.getMessage());
        }
    }

    /**
     * Funktion zum Ermitteln des zu einer MAC zugehörigen Herstellers
     * @param macAddress MAC-Adresse, deren Hersteller ermittelt werden soll im Format 00:00:00:00:00:00
     * @return String mit dem Herstellernamen
     */
    public String getVendorName(String macAddress)
    {
        if(macAddress.equals(""))
            return "Keine Angabe";
        else
        {
            String[] splittetMac = macAddress.split(":");
            String mac = splittetMac[0] + "-" + splittetMac[1] + "-" + splittetMac[2];
            if(vendors.size() > 0)
            {
                for(String s: vendors)
                {
                    if(s.contains(mac))
                        return s.substring(s.lastIndexOf(";") + 1);
                }
            }
        }
        return "keine Angabe";
    }

    /**
     * Funktion zum Ermitteln, ob es sich um ein Telefon oder ein anderes Gerät mit Bluetooth handelt.
     * @param vendor Herstellername
     * @return 1, wenn das Gerät ein Telefon ist, sonst 0
     */
    public String getPhonetype(String vendor)
    {
        for(String str: phoneNames)
        {
            if(vendor.toLowerCase().contains(str.toLowerCase()))
                return "1";
        }
        return "0";
    }
}
