package cl.app.bluetoothlogger.gps;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import cl.app.bluetoothlogger.R;

/**
 * Klasse für die Implementierung des GPSListeners
 * @author leitzke
 */
public class GPSListener implements LocationListener 
{
	private final Activity activity;
    private final currentLocation cl;

    public static final int MAX_NUMBER_OF_UPDATES = 10;

    public GPSListener(Activity activity, currentLocation cl)
    {
        this.activity = activity;
        this.cl = cl;
    }
    
    @Override
    public void onLocationChanged(Location loc) 
    {
    	if (loc != null)
        {
            double speed = (double)loc.getSpeed();
            speed = speed * 3.6;
            speed = Math.round(10.0 * speed) / 10.0;
            cl.setCoordinates(loc.getLatitude(), loc.getLongitude(), speed);
        }

        else
            Log.i(activity.getString(R.string.app_name), "Location null");
    }
    	
    @Override
    public void onProviderDisabled(String provider) 
    {
        Toast.makeText(activity, "Gps deaktiviert", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProviderEnabled(String provider) 
    {
        Toast.makeText(activity, "Gps aktiviert", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras){}
}