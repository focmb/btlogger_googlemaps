package cl.app.bluetoothlogger.gps;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationListener;
import android.location.LocationManager;

/**
 * Klasse zum Aktivieren der GPS-Funktion
 * @author leitzke
 */
public class GPSManager extends Application
{
	private final Activity activity;
	public LocationManager mlocManager;
	private final currentLocation cl;
	private LocationListener gpsListener;
	
	public GPSManager(Activity activity, currentLocation cl)
	{
		this.activity = activity;
		this.cl = cl;
	}

	public void start()
	{
		mlocManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
		if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) 
		{
            setUp();
            findLoc();
        }
		else 
		{
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(activity);
            alertDialogBuilder
                    .setMessage("GPS ist deaktiviert! Aktivieren?")
                    .setCancelable(false)
                    .setPositiveButton("GPS aktivieren",new DialogInterface.OnClickListener() 
                    {
                    	public void onClick(DialogInterface dialog,int id)
                    	{
                    		Intent callGPSSettingIntent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            activity.startActivity(callGPSSettingIntent);
                        }
                     });
            alertDialogBuilder.setNegativeButton("Abbrechen",new DialogInterface.OnClickListener()
            {
            	public void onClick(DialogInterface dialog, int id) 
            	{
            		dialog.cancel();
                }
            });
            AlertDialog alert = alertDialogBuilder.create();
            alert.show();
        }
	}
	
	/**
	 * GPS-Listener anlegen
	 */
	public void setUp() 
	{
        gpsListener = new GPSListener(activity, cl);
    }

    /**
     * Location bestimmen
     */
    void findLoc()
    {
    	mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, gpsListener); // 0, 0 - minTime, minDistance - 0, 0 braucht am meisten Strom, bringt aber das maximale Ergebnis
        gpsListener.onLocationChanged(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
    }
}
