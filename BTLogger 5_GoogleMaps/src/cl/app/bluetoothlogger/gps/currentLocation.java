package cl.app.bluetoothlogger.gps;

/**
 * Klasse für Getter und Setter der GPS-Variablen
 * @author leitzke
 */
public class currentLocation
{
	private final double[] coordinates = new double[3];
	/**
	 * GPS-Koordinate setzen
	 * @param x - Latitude als double
	 * @param y - Longitude als double
     * @param speed - Speed als float
	 */
	public void setCoordinates(double x, double y, double speed)
	{
		coordinates[0] = x;
		coordinates[1] = y;
        coordinates[2] = speed;
	}
	/**
	 * GPS-Koordinate ausgeben
	 * @return double-Array mit x und y double
	 */
	public double[] getCoordinates()
	{
		return coordinates;
	}
	
	private boolean scanning = false;
	
	/**
	 * aktuellen BT-Scan-Status zurückgeben
	 * @return true, wenn Scan läuft
	 */
	public boolean isScanning()
	{
		return scanning;
	}
	/**
	 * aktuellen BT-Scan-Status setzen
	 * @param scanning - true = Scan läuft, false = Scan läuft nicht
	 */
	public void setScanning(boolean scanning)
	{
		this.scanning = scanning;
	}
}
