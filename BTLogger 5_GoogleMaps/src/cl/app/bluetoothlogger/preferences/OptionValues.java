package cl.app.bluetoothlogger.preferences;

import android.content.SharedPreferences;
import android.graphics.Color;

/**
 * Klasse zum Setzen und Auslesen der Variablen, die durch die Optionen gesetzt werden
 */
public class OptionValues
{
    //region Getter Setter
    private boolean toggleGps = false;

    /**
     * Setzen des GPS-Schalters (Option: GPS-Dialog beim Beenden der App anzeigen)
     * @param toggleGps
     */
    public void setGpsToggle(boolean toggleGps)
    {
        this.toggleGps = toggleGps;
    }

    /**
     * Auslesen des GPS-Schalters
     * @return true, wenn GPS-Dialog angezeigt werden soll, sonst false
     */
    public boolean getGpsToggle()
    {
        return toggleGps;
    }

    private boolean toggleBt = false;

    /**
     * Setzen des BT-Schalters (Option: BT deaktivieren beim Beenden der App)
     * @param toggleBt
     */
    public void setBtToggle(boolean toggleBt)
    {
        this.toggleBt = toggleBt;
    }

    /**
     * Auslesen des BT-Schalters
     * @return true, wenn BT beim Verlassen der App ausgeschaltet werden soll
     */
    public boolean getBtToggle()
    {
        return toggleBt;
    }

    private boolean logOn = false;

    /**
     * Setzen des Schalters für das Dateischreiben
     * @param logOn
     */
    public void setlogOn(boolean logOn)
    {
        this.logOn = logOn;
    }

    /**
     * Auslesen des Schalters für das Dateischreiben
     * @return true, wenn das Logging angeschaltet werden soll
     */
    public boolean getlogOn()
    {
        return logOn;
    }

    private boolean showNew = false;

    /**
     * Setzen des Schalters für die Anzeige neuer Geräte
     * @param showNew
     */
    public void setShowNew(boolean showNew)
    {
        this.showNew = showNew;
    }

    /**
     * Auslesen des Schalters für die Anzeige neuer Geräte
     * @return true, wenn nur neue Geräte angezeigt werden sollen
     */
    public boolean getShowNew()
    {
        return showNew;
    }

    private boolean mapsShowNewOnly = false;

    /**
     * Setzen des Scahlters für die Anzeige nur neuer Geräte in GoogleMaps
     * @param mapsShowNewOnly
     */
    public void setMapsShowNewOnly(boolean mapsShowNewOnly)
    {
        this.mapsShowNewOnly = mapsShowNewOnly;
    }

    /**
     * Auslesen des Schalters für die Anzeige nur neuer Geräte in GoogleMaps
     * @return true, wenn in GoogleMaps nur neue Geräte angezeigt werden sollen
     */
    public boolean getMapsShowNewOnly()
    {
        return mapsShowNewOnly;
    }

    private boolean useDb = false;

    /**
     * Setzen des Schalters für die Nutzung der Datenbank zur Erkennung neuer Geräte
     * @param useDb
     */
    public void setUseDb(boolean useDb)
    {
        this.useDb = useDb;
    }

    /**
     * Auslesen des Schalters für die Nutzung der Datenbank zur Erkennung neuer Geräte
     * @return true, wenn die Datenbank zur Erkennung neuer Geräte verwendet werden soll
     */
    public boolean getUseDb()
    {
        return useDb;
    }

    private boolean toggleVibration = true;

    /**
     * Setzen des Schalters für die Vibration
     * @param toggleVibration
     */
    public void setToggleVibration(boolean toggleVibration)
    {
        this.toggleVibration = toggleVibration;
    }

    /**
     * Auslesen des Schalters für die Vibration
     * @return True, wenn Vibration bei Geräteerkennung aktiv
     */
    public boolean getToggleVibration()
    {
        return toggleVibration;
    }

    private boolean keepDisplayOn = true;

    /**
     * Setzen des Schalters für die Option zum Angeschaltetlassen des Displays
     * @param keepDisplayOn boolean-Wert für den gewünschten Displaystatus
     */
    public void setKeepDisplayOn(boolean keepDisplayOn)
    {
        this.keepDisplayOn = keepDisplayOn;
    }

    /**
     * Auslesen des Schalters für die Option zum Angeschaltetlassen des Displays
     */
    public boolean isKeepDisplayOn()
    {
        return keepDisplayOn;
    }

    private String mapsViewType = "";

    /**
     * Setzen des Ansichtstyps für GoogleMaps
     * @param mapsViewType
     */
    public void setMapsViewType(String mapsViewType)
    {
        this.mapsViewType = mapsViewType;
    }

    /**
     * Auslesen des Ansichtstyps für GoogleMaps
     * @return String mit Ansichtsoption (Karte, Satellit, Satellit mit Label)
     */
    public String getMapsViewType()
    {
        return mapsViewType;
    }

    private String scanType = "";

    /**
     * Setzen des Scantyps
     * @param scanType
     */
    public void setScanType(String scanType)
    {
        this.scanType = scanType;
    }

    /**
     * Auslesen des Scantyps
     * @return String mit dem Scantyp (MAC, Name, MAC + Name)
     */
    public String getScanType()
    {
        return scanType;
    }


    private String roadType = "0";

    /**
     * Setzen des Straßentyps
     * @param roadType
     */
    public void setRoadType(String roadType)
    {
        this.roadType = roadType;
    }

    /**
     * Auslesen des Straßentyps
     * @return String mit der Item-Nummer der Auswahlliste für den Straßentyp
     */
    public String getRoadtype()
    {
        return roadType;
    }

    private String scrollType = "1";

    /**
     * Setzen der Scrollrichtung
     * @param scrollType
     */
    public void setScrollType(String scrollType)
    {
        this.scrollType = scrollType;
    }

    /**
     * Auslesen der Scrollrichtung
     * @return String mit der Nummer des Scrollrichtung (1 von oben nach unten, 2 von unten nach oben)
     */
    public String getScrollType()
    {
        return scrollType;
    }

    private float newDevSlider = 0.5f;

    /**
     * Setzen der Farbe im Auswahlschieber für neue Geräte
     * @param newDevSlider
     */
    public void setNewDevSlider(float newDevSlider)
    {
        this.newDevSlider = newDevSlider;
    }

    /**
     * Auslesen der Farbe im Auswahlschieber für neue Geräte
     * @return float für die Vibrationsdauer bei Erkennung neuer Geräte
     */
    public float getNewDevSlider()
    {
        return newDevSlider;
    }

    private float oldDevSlider = 0.25f;

    /**
     * Setzen der Farbe im Auswahlschieber für bekannte Geräte
     * @param oldDevSlider
     */
    public void setOldDevSlider(float oldDevSlider)
    {
        this.oldDevSlider = oldDevSlider;
    }
    /**
     * Auslesen der Farbe im Auswahlschieber für bekannte Geräte
     * @return float für die Vibrationsdauer bei Erkennung bekannter Geräte
     */
    public float getOldDevSlider()
    {
        return oldDevSlider;
    }

    private int textNewDevColor = Color.RED;

    /**
     * Setzen der Textfarbe in der Listview für neue Geräte
     * @param textNewDevColor int-Wert für die Farbe bekannter Geräte
     */
    public void setTextNewDevColor(int textNewDevColor)
    {
        this.textNewDevColor = textNewDevColor;
    }

    /**
     * Auslesen der Textfarbe in der List view für neue Geräte
     * @return int mit dem Farbwert für die TExtfarbe neuer Geräte
     */
    public int getTextNewDevColor()
    {
        return textNewDevColor;
    }

    private int textOldDevColor = Color.BLACK;

    /**
     * Setzen der Textfarbe in der Listview für bekannte Geräte
     * @param textOldDevColor int-Wert für die Farbe erkannter Geräte
     */
    public void setTextOldevColor(int textOldDevColor)
    {
        this.textOldDevColor = textOldDevColor;
    }
    /**
     * Auslesen der Textfarbe in der List view für bekannte Geräte
     * @return int mit dem Farbwert für die TExtfarbe bekannter Geräte
     */
    public int getTextOldDevColor()
    {
        return textOldDevColor;
    }


    private boolean useVendor = false;

    /**
     * Setzen des Schalters für das Auslesen des Chipherstellers
     * @param useVendor
     */
    public void setUseVendor(boolean useVendor)
    {
        this.useVendor = useVendor;
    }

    /**
     * Auslesen des Schalters für die Nutzung der Datenbank zur Erkennung neuer Geräte
     * @return true, wenn die Datenbank zur Erkennung neuer Geräte verwendet werden soll
     */
    public boolean getUseVendor()
    {
        return useVendor;
    }

    private boolean useIgnore = false;

    /**
     * Setzen des Schalters für das Auslesen des Chipherstellers
     * @param useIgnore
     */
    public void setUseIgnore(boolean useIgnore)
    {
        this.useIgnore = useIgnore;
    }

    /**
     * Auslesen des Schalters für die Nutzung der Datenbank zur Erkennung neuer Geräte
     * @return true, wenn die Datenbank zur Erkennung neuer Geräte verwendet werden soll
     */
    public boolean getUseIgnore()
    {
        return useIgnore;
    }

    private boolean useRow = true;

    /**
     * Setzen des Schalters für das zeilenweise Anzeigen der Geräteinformationen
     * @param useRow
     */
    public void setUseRow(boolean useRow)
    {
        this.useRow = useRow;
    }

    /**
     * Auslesen des Schalters für die Nutzung der Datenbank zur Erkennung neuer Geräte
     * @return true, wenn die Datenbank zur Erkennung neuer Geräte verwendet werden soll
     */
    public boolean getUseRow()
    {
        return useRow;
    }

    private boolean useId = false;

    /**
     * Setzen des Schalters für das Mitloggen der Id
     * @param useId
     */
    public void setUseId(boolean useId)
    {
        this.useId = useId;
    }

    /**
     * Auslesen des Schalters für das Mitloggen der Id
     * @return true, wenn die Id mit in das Logfile geschrieben werden soll
     */
    public boolean getUseId()
    {
        return useId;
    }

    //endregion

    private final SharedPreferences prefs;

    /**
     * Auflistung der Preference-Namen für den String-Switch-Case
     */
    public enum options
    {
        TOGGLE_GPS,
        TOGGLE_BT,
        LOG_ON,
        TOGGLE_SHOW_NEW,
        GM_VIEW_NEW_ONLY,
        USE_DB,
        GM_VIEW_TYPE,
        SCAN_TYPE,
        NEW_DEV_SLIDER,
        OLD_DEV_SLIDER,
        SCROLL_TYPE,
        TOGGLE_VIBRATION,
        SET_TEXT_NEW_DEV_COLOR,
        SET_TEXT_OLD_DEV_COLOR,
        TOGGLE_DISPLAY,
        USE_VENDOR,
        USE_IGNORE,
        USE_ROW,
        USE_ID,
        ROAD_TYPE
    }

    public OptionValues(SharedPreferences prefs)
    {
        this.prefs = prefs;
    }

    /**
     * Setzen der Variablen für die Preferences beim Programmstart
     */
    public void setVariables()
    {
        setGpsToggle(prefs.getBoolean("TOGGLE_GPS", true));
        setBtToggle(prefs.getBoolean("TOGGLE_BT", true));
        setlogOn(prefs.getBoolean("LOG_ON", true));
        setShowNew(prefs.getBoolean("TOGGLE_SHOW_NEW", true));
        setMapsShowNewOnly(prefs.getBoolean("GM_VIEW_NEW_ONLY", true));
        setUseDb(prefs.getBoolean("USE_DB", false));
        setKeepDisplayOn(prefs.getBoolean("TOOGLE_DISPLAY", true));
        setMapsViewType(prefs.getString("GM_VIEW_TYPE", "1"));
        setScanType(prefs.getString("SCAN_TYPE", "1"));
        setNewDevSlider(prefs.getFloat("NEW_DEV_SLIDER", 0.5f));
        setOldDevSlider(prefs.getFloat("OLD_DEV_SLIDER", 0.25f));
        setScrollType(prefs.getString("SCROLL_TYPE", "1"));
        setToggleVibration(prefs.getBoolean("TOGGLE_VIBRATION", true));
        setTextNewDevColor(prefs.getInt("SET_TEXT_NEW_DEV_COLOR", Color.RED));
        setTextOldevColor(prefs.getInt("SET_TEXT_OLD_DEV_COLOR", Color.BLACK));
        setUseVendor(prefs.getBoolean("USE_VENDOR", false));
        setUseIgnore(prefs.getBoolean("USE_IGNORE", false));
        setUseRow(prefs.getBoolean("USE_ROW", true));
        setUseId(prefs.getBoolean("USE_ID", false));
        setRoadType(prefs.getString("ROAD_TYPE", "0"));
    }
}
