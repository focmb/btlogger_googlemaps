package cl.app.bluetoothlogger.preferences;

import cl.app.bluetoothlogger.R;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;

/**
 * Klasse zum Einbinden der SaredPreferences (Einstellungen, die gespeichert werden)
 * @author leitzke
 */
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class AppPreferences extends PreferenceActivity
{
	private final static int prefs = R.xml.preferences;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
        try 
        {
            getClass().getMethod("getFragmentManager");
            AddResourceApi11AndGreater();
        }
        catch (NoSuchMethodException e)
        {
            AddResourceApiLessThan11();
        }
	}

	@SuppressWarnings("deprecation")
    void AddResourceApiLessThan11()
	{
	    addPreferencesFromResource(prefs);
	}
	
	void AddResourceApi11AndGreater()
	{
	    getFragmentManager().beginTransaction().replace(android.R.id.content, new PF()).commit();
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class PF extends PreferenceFragment
	{       
	    public void onCreate(final Bundle savedInstanceState)
	    {
	        super.onCreate(savedInstanceState);
	        addPreferencesFromResource(AppPreferences.prefs);
	    }
	}
}
