/*
 * Copyright 2012 Jay Weisskopf
 *
 * Licensed under the MIT License (see LICENSE.txt)
 */

package cl.app.bluetoothlogger.preferences;

import cl.app.bluetoothlogger.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

/**
 * @author Jay Weisskopf 
 * adjusted by Christian Leitzke
 */
public class SliderPreference extends DialogPreference
{
    private final static int SEEKBAR_RESOLUTION = 1000;

    private float mValue;
    private int mSeekBarValue;
    private CharSequence[] mSummaries;
    private TextView tv_info, tv_main;

    public SliderPreference(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        setup(context, attrs);
    }

    public SliderPreference(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        setup(context, attrs);
    }

    private void setup(Context context, AttributeSet attrs)
    {
        setDialogLayoutResource(R.layout.slider_preference_dialog);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SliderPreference);
        try
        {
            setSummary(a.getTextArray(R.styleable.SliderPreference_android_summary));
        }
        catch (Exception e)
        {
            Log.d("BTLoger4", e.getMessage());
        }
        if(a != null)
            a.recycle();
    }

    @Override
    protected Object onGetDefaultValue(TypedArray a, int index)
    {
        return a.getFloat(index, 0);
    }

    @Override
    protected void onSetInitialValue(boolean restoreValue, Object defaultValue)
    {
        setValue(restoreValue ? getPersistedFloat(mValue) : (Float) defaultValue);
    }

    @Override
    public CharSequence getSummary()
    {
        if (mSummaries != null && mSummaries.length > 0)
        {
            float x = mValue * SEEKBAR_RESOLUTION;
            String y = String.valueOf(x);
            mSummaries[0] = y.substring(0, y.lastIndexOf(".")) + " Millisekunden";
            return mSummaries[0];
        }
        else
            return super.getSummary();
    }

    void setSummary(CharSequence[] summaries)
    {
        mSummaries = summaries;
    }

    @Override
    public void setSummary(CharSequence summary)
    {
        super.setSummary(summary);
        mSummaries = null;
    }

    @Override
    public void setSummary(int summaryResId)
    {
        try
        {
            setSummary(getContext().getResources().getStringArray(summaryResId));
        }
        catch (Exception e)
        {
            super.setSummary(summaryResId);
        }
    }

    public float getValue()
    {
        return mValue;
    }

    void setValue(float value)
    {
        value = Math.max(0.1f, Math.min(value, 1));
        if (shouldPersist())
            persistFloat(value);
        if (value != mValue)
        {
            mValue = value;
            notifyChanged();
        }
    }

    @Override
    protected View onCreateDialogView()
    {
        mSeekBarValue = (int) (mValue * SEEKBAR_RESOLUTION);
        final View view = super.onCreateDialogView();
        SeekBar seekbar = (SeekBar) view.findViewById(R.id.slider_preference_seekbar);
        seekbar.setMax(SEEKBAR_RESOLUTION);
        seekbar.setProgress(mSeekBarValue);
        tv_info = (TextView)view.findViewById(R.id.textView);
        tv_info.setTextColor(Color.WHITE);
        tv_info.setText(SliderPreference.this.mSeekBarValue+" Millisekunden");
        tv_main = (TextView)view.findViewById(R.id.textView2);
        tv_main.setTextColor(Color.WHITE);
        tv_main.setText("Vibrationsdauer zwischen 100 und 1000 Millisekunden");
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener()
        {
            @Override
            public void onStopTrackingTouch(SeekBar seekBar){}

            @Override
            public void onStartTrackingTouch(SeekBar seekBar){}

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)
            {
                if (fromUser)
                {
                    if(progress < 100)
                        progress = 100;
                    SliderPreference.this.mSeekBarValue = progress;
                    tv_info.setText(SliderPreference.this.mSeekBarValue+" Millisekunden");
                }
            }
        });
        return view;
    }

    @Override
    protected void onDialogClosed(boolean positiveResult)
    {
        final float newValue = (float) mSeekBarValue / SEEKBAR_RESOLUTION;
        if (positiveResult && callChangeListener(newValue))
            setValue(newValue);
        super.onDialogClosed(positiveResult);
    }
}
