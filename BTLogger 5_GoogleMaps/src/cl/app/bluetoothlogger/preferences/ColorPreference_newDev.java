/**
 * Klasse für den ColorPicker in den Preferences
 */

package cl.app.bluetoothlogger.preferences;

import android.annotation.SuppressLint;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class ColorPreference_newDev extends DialogPreference
{
	private int color = 0;

	public interface OnColorChangedListener
	{
		void colorChanged(int color);
	}
	
	public ColorPreference_newDev(Context context, AttributeSet attrs) 
	{
		super(context, attrs);
	}
		
	protected void onDialogClosed(boolean positiveResult) 
	{
		if (positiveResult && color != 0) 
			persistInt(color);
		super.onDialogClosed(positiveResult);
	}

	protected void onPrepareDialogBuilder(Builder builder) 
	{
		new OnColorChangedListener()
		{
			public void colorChanged(int c) 
			{
				ColorPreference_newDev.this.color = c;
			}
		};
		int color = getSharedPreferences().getInt("SET_TEXT_NEW_DEV_COLOR", Color.BLACK);
		builder.setView(new ColorPickerView(getContext(), color));
		super.onPrepareDialogBuilder(builder);
	}
	
	@SuppressLint("DrawAllocation")
	class ColorPickerView extends View
	{
		static final int CENTER_X = 100;
		static final int CENTER_Y = 100;
		static final int CENTER_RADIUS = 32;

		Paint paint = null;
		Paint centerPaint = null;
		final int[] colors = new int[] {0xFFFF0000, 0xFFFF00FF, 0xFF575757, 0xFF000000, 0xFF8A8A8A, 0xFF0000FF, 0xFF00FFFF, 0xFF00FF00, 0xFFFFFF00, 0xFFFF0000};

		ColorPickerView(Context context, int color)
		{
			super(context);
			paint = new Paint(Paint.ANTI_ALIAS_FLAG);
			paint.setShader(new SweepGradient(0, 0, colors, null));
			paint.setStyle(Paint.Style.STROKE);
			paint.setStrokeWidth(50);
			centerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			centerPaint.setColor(color);
			centerPaint.setStrokeWidth(80);
		}
		
		protected void onDraw(Canvas canvas) 
		{
			int centerX = getRootView().getWidth()/2 - (int)(paint.getStrokeWidth()/2);
			float r = CENTER_X - paint.getStrokeWidth()*0.5f;
			canvas.translate(centerX, CENTER_Y);
			canvas.drawOval(new RectF(-r, -r, r, r), paint);
			canvas.drawCircle(0, 0, CENTER_RADIUS, centerPaint);
		}
		
		protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) 
		{
			int width = getRootView().getWidth();
			if (width == 0) 
				width = CENTER_X*2 + 50;
			setMeasuredDimension(width, CENTER_Y*2);
		}
		
		private int ave(int s, int d, float p) 
		{
			return s + Math.round(p * (d - s));
		}
		
		private int interpColor(int colors[], float unit) 
		{
			if (unit <= 0) 
				return colors[0];
			if (unit >= 1) 
				return colors[colors.length - 1];
			float p = unit * (colors.length - 1);
			int i = (int) p;
			p -= i;
			// now p is just the fractional part [0...1) and i is the index
			int c0 = colors[i];
			int c1 = colors[i + 1];
			int a = ave(Color.alpha(c0), Color.alpha(c1), p);
			int r = ave(Color.red(c0), Color.red(c1), p);
			int g = ave(Color.green(c0), Color.green(c1), p);
			int b = ave(Color.blue(c0), Color.blue(c1), p);
			return Color.argb(a, r, g, b);
		}
		
		public boolean onTouchEvent(MotionEvent event) 
		{
			float x = event.getX() - getRootView().getWidth()/2;
			float y = event.getY() - CENTER_Y;
			switch (event.getAction()) 
			{
				case MotionEvent.ACTION_DOWN:
				case MotionEvent.ACTION_MOVE:
					float angle = (float) java.lang.Math.atan2(y, x);
					float unit = (float)(angle / (2 * Math.PI));
					if (unit < 0)
						unit += 1;
					centerPaint.setColor(interpColor(colors, unit));
					invalidate();
					break;
				case MotionEvent.ACTION_UP:
					ColorPreference_newDev.this.color = centerPaint.getColor();
					break;
			}
			return true;
		}
	}
}
