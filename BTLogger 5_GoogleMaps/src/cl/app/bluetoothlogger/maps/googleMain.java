/*************************************************
 * Klasse zum Einbinden von GoogleMaps und		 *
 * Anzeigen von Geräte-Koordinaten auf der Karte *
 * @author: Christian Leitzke					 *	
 *************************************************/

package cl.app.bluetoothlogger.maps;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;
import cl.app.bluetoothlogger.R;
import cl.app.bluetoothlogger.filechooser.utils.FileUtils;
import cl.app.bluetoothlogger.utilities.StringArrayComparator;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.common.base.Joiner;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

@SuppressWarnings("ConstantConditions")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class googleMain extends FragmentActivity
{
    private GoogleMap gMap;
    private String viewOption = "";
    private StringArrayComparator sac;

    public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        sac = new StringArrayComparator();
        ArrayList<String> coords = getIntent().getExtras().getStringArrayList("coordinates");
        viewOption = getIntent().getExtras().getString("viewOption");
        LocationManager locManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        SupportMapFragment smf = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        try
    	{
    		int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
    		if(status != ConnectionResult.SUCCESS)
        	{
        		int requestCode = 10;
        		Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
        		dialog.show();
        	}
        	else
        	{
        		if(!locManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                {
                	AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
                    alertbox.setMessage("GPS ist deaktiviert, eigene Position kann nicht ermittelt werden! - zurück zum Hauptfenster?");
                	alertbox.setPositiveButton("Ja", new DialogInterface.OnClickListener() 
                	{
                		public void onClick(DialogInterface arg0, int arg1) 
                		{
                			try
                			{
                				finish();
                			}
                			catch(Exception ex)
                			{
                				Log.e(getString(R.string.app_name), ex.getMessage());
                			}
                		}
                	});
                	alertbox.setNegativeButton("Nein", new DialogInterface.OnClickListener() 
                	{
                		public void onClick(DialogInterface arg0, int arg1) 
                		{
                			Toast.makeText(googleMain.this, "Aktion abgebrochen!", Toast.LENGTH_SHORT).show();
                		}
                	});
                	alertbox.show();
                }
        	}
    		gMap = smf.getMap();
    		setupMapView(gMap);
    		showLocations(coords);
    	}
    	catch(Exception ex)
    	{
    		Log.d(getString(R.string.app_name), ex.getMessage());
    	}
    }
	
	/**
	 * Anpassen der Google Maps Oberfläche und Funktionen
	 * @param _map Karte, für die die Funktionen und Ansicht angepasst werden soll
	 */
	private void setupMapView(GoogleMap _map)
	{
		if(viewOption.equals("1"))
        	_map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		else if(viewOption.equals("2"))
			_map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        else
        	_map.setMapType(GoogleMap.MAP_TYPE_HYBRID);
		UiSettings settings = gMap.getUiSettings();
		settings.setAllGesturesEnabled(true);
		settings.setCompassEnabled(true);
		settings.setMyLocationButtonEnabled(true);
		settings.setRotateGesturesEnabled(true);
		settings.setScrollGesturesEnabled(true);
		settings.setTiltGesturesEnabled(true);
		settings.setZoomControlsEnabled(true);
		settings.setZoomGesturesEnabled(true);
	}

    /**
     * Setzen der Marker für die übergebenen Datensätze
     * @param dataToShow Liste mit den Daten der Geräte, die auf der Karte angezeigt werden sollen
     */
	private void showLocations(ArrayList<String> dataToShow)
	{
		if(dataToShow != null)
		{
			int i = 1;
			for(String s: dataToShow)
			{
				String[] _s = s.split(" ");
                for(int j = 0; j < _s.length; j ++)
                {
                    if(_s[j].contains("<br>"))
                        _s[j] = _s[j].replace("<br>", "");
                    if(_s[j].contains("<b>"))
                        _s[j] = _s[j].replace("<b>", "");
                    if(_s[j].contains("</b>"))
                        _s[j] = _s[j].replace("</b>", "");
                }
				String description = "";
				String time = "";
				double lat = 0.0;
				double lon = 0.0;
				for(int j = 0; j < _s.length; j ++)
				{
					if(_s[j].contains("Lat:"))
					{
						lat = Double.parseDouble(_s[j +1]);
						int k = 1;
						while(k < j)
						{
							description += _s[k] + " ";
							k ++;
						}
					}
					if(_s[j].contains("Long:"))
						lon = Double.parseDouble(_s[j + 1]);
					if(_s[j].contains("Zeit:"))
					{
						time = _s[j + 2];
					}
				}
				if(lat != 0.0 && lon != 0.0)
				{
					gMap.addMarker(new MarkerOptions()
						.position(new LatLng(lat, lon))
						.title("Gerät: " + i))
						.setSnippet(description + "\n" + " Zeit: " + time);
					if(i == 1)
					{
						gMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(lat, lon)));
						gMap.animateCamera(CameraUpdateFactory.zoomTo(15));
					}
				}
				i ++;
			}
		}
		else
		{
			gMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(51.055207, 10.063477)));
			gMap.animateCamera(CameraUpdateFactory.zoomTo(6));
			Toast.makeText(getApplicationContext(), "Keine Daten zum Anzeigen auf der Karte vorhanden!", Toast.LENGTH_SHORT).show();	
		}	
	}
	
	/**
	 * Optionsmenüeintrage hinzufügen
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(1, 196, 0, "Satellit mit Label");
		menu.add(1, 197, 0, "Karte");
		menu.add(1, 198, 0, "Satellit");
		menu.add(1, 199, 0, "Karte zurücksetzen");
		menu.add(1, 200, 0, "Log-Datei einlesen");
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Optionsmenüeinträge mit Funktion versehen
	 */
	@Override
	public boolean onOptionsItemSelected (MenuItem item)
	{
		switch (item.getItemId())
		{
			case 196:
				gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
				return true;
			case 197:
	        	gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);	
	        	return true;
	        case 198 :
	        	gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
	        	return true;
	        case 199:
	        	resetMap();
	        	return true;
	        case 200:
                Intent target = FileUtils.createGetContentIntent();
                Intent intent = Intent.createChooser(target, getString(R.string.chooser_title));
                try
                {
                    startActivityForResult(intent, REQUEST_CODE);
                }
                catch (ActivityNotFoundException e)
                {
                    Log.e(getString(R.string.app_name), e.getMessage());
                }
                return true;
	    }
	    return false;
	}

    /**
     * Zurücksetzen der Karte auf Ursprungsansicht
     */
    private void resetMap()
    {
        gMap.clear();
        if(viewOption.equals("1"))
            gMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        else if(viewOption.equals("2"))
            gMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        else
            gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        gMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(51.055207, 10.063477)));
        gMap.animateCamera(CameraUpdateFactory.zoomTo(6));
    }

    /**
     * Einlesen der Datei mit den erkannten Geräten
     * @param file Uri mit dem Dateipfad
     * @return Liste mit den eingelesenen Daten
     */
    private ArrayList<String> readFile(File file)
	{
        ArrayList<String> devices = null;
        FileInputStream fis = null;
        InputStreamReader input = null;
        BufferedReader buf = null;
        try
        {
            devices = new ArrayList<String>();
            fis = new FileInputStream(file);
            input = new InputStreamReader(fis);
            buf = new BufferedReader(input);
            String line;
            int lineCount = 0;
            String[] header = new String[7];
            int type = 0;
            while((line = buf.readLine()) != null)
            {
                if(lineCount == 0)
                {
                    header = line.split(";");
                    if(!header[0].equals("Id"))
                        type = 1;
                }
                else
                {
                    String[] data = line.split(";");
                    switch(type)
                    {
                        case 0:
                            if(Double.parseDouble(data[3]) != 0.0 && Double.parseDouble(data[4]) != 0.0)
                            {
                                String deviceInfo = "";
                                for(int i = 0; i < data.length; i++)
                                {
                                    deviceInfo += header[i] + ": " + data[i] + " ";
                                }
                                devices.add(deviceInfo);
                            }
                            break;
                        case 1:
                            if(Double.parseDouble(data[2]) != 0.0 && Double.parseDouble(data[3]) != 0.0)
                            {
                                String deviceInfo = "";
                                for(int i = 0; i < data.length; i++)
                                {
                                    deviceInfo += header[i] + ": " + data[i] + " ";
                                }
                                devices.add(deviceInfo);
                            }
                            break;
                    }
                }
                lineCount++;
            }
        }
        catch (FileNotFoundException e)
        {
            Log.e(getString(R.string.app_name), "FileNotFound-Fehler", e);
        }
        catch (IOException e)
        {
            Log.e(getString(R.string.app_name), "IO-Fehler", e);
        }
        finally
        {
            try
            {
                if(buf != null)
                    buf.close();
                if(input != null)
                    input.close();
                if(fis != null)
                    fis.close();
            }
            catch (IOException e)
            {
                Log.e(getString(R.string.app_name), "IO-Fehler", e);
            }
        }
        return devices;
    }
	
	private static final int REQUEST_CODE = 6384; // onActivityResult request code
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		switch (requestCode) 
		{
			case REQUEST_CODE:	
			if (resultCode == RESULT_OK)
			{		
				if (data != null)
				{
					final Uri uri = data.getData();
					try 
					{
						final File file = FileUtils.getFile(uri);
						resetMap();
                        final ArrayList<String> PointsToAdd = readFile(file);
                        if(PointsToAdd.size() > 0)
                        {
                            String tmpDev = " gültiger Datensatz gefunden";
                            if(PointsToAdd.size() > 1)
                            {
                                tmpDev = " gültige Datensätze gefunden";
                            }
                            AlertDialog.Builder deviceQuestion = new AlertDialog.Builder(this);
                            deviceQuestion.setTitle(PointsToAdd.size() + tmpDev);
                            deviceQuestion.setMessage("Welche Geräte sollen angezeigt werden?");
                            deviceQuestion.setPositiveButton("Alle", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface arg0, int arg1)
                                {
                                    showLocations(PointsToAdd);
                                }
                            });
                            deviceQuestion.setNegativeButton("nur Neue", new DialogInterface.OnClickListener()
                            {
                                public void onClick(DialogInterface arg0, int arg1)
                                {
                                    String[][] colCount = new String[PointsToAdd.size()][];
                                    int macAddress = 0;
                                    for (int i = 0; i < PointsToAdd.size(); i++)
                                    {
                                        String[] _col = PointsToAdd.get(i).split(" ");
                                        if (i == 0)
                                        {
                                            for (int j = 0; j < _col.length; j++)
                                            {
                                                if (_col[j].equals("MAC:"))
                                                {
                                                    macAddress = j + 1;
                                                    break;
                                                }
                                            }
                                        }
                                        colCount[i] = _col;
                                    }
                                    sac.setSortColumn(macAddress);
                                    Arrays.sort(colCount, sac);
                                    String oldMac = "00:00:00:00:00:00";
                                    ArrayList<String> singleDev = new ArrayList<String>();
                                    for (String[] aColCount : colCount)
                                    {
                                        if (!aColCount[macAddress].equals(oldMac))
                                        {
                                            singleDev.add(Joiner.on(" ").join(aColCount));
                                            oldMac = aColCount[macAddress];
                                        }
                                    }
                                    showLocations(singleDev);
                                }
                            });
                            deviceQuestion.show();
                        }
                        else
                        {
                            Toast.makeText(this, "Kein gültiger Datensatz gefunden", Toast.LENGTH_SHORT).show();
                        }
					}
					catch (Exception e) 
					{
						Log.e(getString(R.string.app_name), "Dateiauswahlfehler", e);
					}
				}
			} 
			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}

