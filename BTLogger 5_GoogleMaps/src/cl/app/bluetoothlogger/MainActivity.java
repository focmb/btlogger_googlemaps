package cl.app.bluetoothlogger;

import android.annotation.SuppressLint;
import android.app.*;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.*;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.provider.BaseColumns;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.Html;
import android.util.Log;
import android.view.*;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.widget.*;
import cl.app.bluetoothlogger.gps.GPSListener;
import cl.app.bluetoothlogger.gps.GPSManager;
import cl.app.bluetoothlogger.gps.currentLocation;
import cl.app.bluetoothlogger.maps.googleMain;
import cl.app.bluetoothlogger.preferences.AppPreferences;
import cl.app.bluetoothlogger.preferences.OptionValues;
import cl.app.bluetoothlogger.utilities.getVendorInformation;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Christian Leitzke
 * @version 12.05.2014
 */
@SuppressLint("SimpleDateFormat")
public class MainActivity extends Activity implements OnSharedPreferenceChangeListener
{
	private ArrayAdapter<String> adapter;
    private ArrayList<String>excluded;
	private ArrayList<String> dataList;
	private BluetoothAdapter btadapter;
	private BTBondReceiver btr;
	private Button startScan;
    private Spinner spinner;
	private currentLocation cl;
	private DateFormat df;
	private DBFunctions dbf;
    private getVendorInformation gvi;
	private File root, file;
	private GPSManager gps;
	private GPSListener lis;
    private OptionValues ov;
	private ListView lv;
    private TextView tv;
    private TextView tv2;
	private NotificationCompat.Builder ncb;
	private NotificationManager noteman;
	private Vibrator mVibrator;
	private int countItems = 0;
    private int  countNewDev = 0;
    private int apiLevel = 0;
    private String fileName;
    String fileNameExclude = "IgnoreList";
    private static final int notificationID = 1;
    private static final int REQUEST_ENABLE_BT = 1;
    private SharedPreferences prefs;
    private OnSharedPreferenceChangeListener myPrefListner;  //Kann nicht local gemacht werden, da sonst der Listener nicht mehr funktioniert

    private String rType = "";

    public String getrType()
    {
        return rType;
    }

    public void setrType(String rType)
    {
        this.rType = rType;
    }



    @Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        initializeVariables();
		initializeNotificationManager();
		initializeStartButton();
        initializeSpinnerListener();
		createDatabaseOnstartup();
        initializePreferenceChangeListener();
        makeLogFolder();
		initialzeBluetoothReceiver();
		checkBTState();
    }
	
	/**
	 * Datenbank anlegen, falls diese nicht existiert
	 */
	private void createDatabaseOnstartup()
	{
		if(!dbf.isTableExists())
			dbf.onCreateDBAndDBTable();
	}
	
	/**
	 * Variablen und Klassen anlegen
	 */
	@SuppressLint("CommitPrefEdits")
	private void initializeVariables()
	{
		apiLevel = Build.VERSION.SDK_INT;
        gvi = new getVendorInformation(this);
        lv = (ListView)findViewById(R.id.listView1);
        int[] dividerColors = {0, 0xFF616161, 0};
		lv.setDivider(new GradientDrawable(Orientation.RIGHT_LEFT, dividerColors));
		lv.setDividerHeight(1);
        tv = (TextView)findViewById(R.id.textView);
        tv2 = (TextView)findViewById(R.id.textView2);
        dataList = new ArrayList<String>();
        File file = getBaseContext().getFileStreamPath(fileNameExclude);
        if(!file.exists())
            excluded = new ArrayList<String>();
        else
            excluded = readIgnoreFile(fileNameExclude);
        adapter = new CustomListAdapter(getApplicationContext(), R.layout.custom_list, dataList);
		lv.setAdapter(adapter);
        lv.setClickable(true);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l)
            {
                showMacVendor(position);
            }
        });
        registerForContextMenu(lv);
		mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        ov = new OptionValues(prefs);
        dbf = new DBFunctions(this);
		cl = new currentLocation();
		gps = new GPSManager(this, cl);
		lis = new GPSListener(this, cl);
 		gps.start();
        if(ov.isKeepDisplayOn())
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}

    /**
     * Einlesen der Ignorierliste
     * @param _file Dateiname
     * @return ArrayListe mit den eingelesenen Zeilen
     */
    private ArrayList<String> readIgnoreFile(String _file)
    {
        ArrayList<String> iL = new ArrayList<String>();
        try
        {
            FileInputStream fin = openFileInput(_file);
            DataInputStream in = new DataInputStream(fin);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String line;
            while ((line = br.readLine()) != null)
            {
                iL.add(line);
            }
            in.close();
            return iL;
        }
        catch (FileNotFoundException ioex)
        {
            Log.e(getString(R.string.app_name), "IO-Fehler 1", ioex);
        }
        catch (OptionalDataException ioex)
        {
            Log.e(getString(R.string.app_name), "IO-Fehler 2", ioex);
        }
        catch (StreamCorruptedException ioex)
        {
            Log.e(getString(R.string.app_name), "IO-Fehler 3", ioex);
        }
        catch (IOException ioex)
        {
            Log.e(getString(R.string.app_name), "IO-Fehler 4", ioex);
        }
        return null;
    }

    /**
     * Schreiben der Ignorierliste in eine Datei
     * @param _file Dateiname der Datei, in welche die Liste geschrieben werden soll
     */
    private void appendIgnoreFile(String _file, String toWrite, ArrayList<String> list)
    {
        if(_file != null && toWrite != null && list == null)
        {
            try
            {
                FileOutputStream fos = openFileOutput(_file, Context.MODE_APPEND);
                OutputStreamWriter osw = new OutputStreamWriter(fos);
                toWrite += "\n";
                osw.append(toWrite);
                osw.flush();
                osw.close();
                fos.close();
            }
            catch (FileNotFoundException e)
            {
                Log.e(getString(R.string.app_name), "Dateiauswahlfehler", e);
            }
            catch(IOException ioex)
            {
                Log.e(getString(R.string.app_name), "Ein/Ausgabefehler", ioex);
            }
        }
        else if(_file != null && list != null && toWrite == null)
        {
            try
            {
                FileOutputStream fos = openFileOutput(_file, Context.MODE_PRIVATE);
                OutputStreamWriter osw = new OutputStreamWriter(fos);
                for(String s: list)
                {
                    s += "\n";
                    osw.append(s);
                    osw.flush();
                }
                osw.close();
                fos.close();
            }
            catch (FileNotFoundException e)
            {
                Log.e(getString(R.string.app_name), "FileNotFound-Fehler", e);
            }
            catch(IOException ioex)
            {
                Log.e(getString(R.string.app_name), "IO-Fehler 5", ioex);
            }
        }
        else
            Log.e(getString(R.string.app_name), "Ignorierdatei kann nicht bearbeitet werden");
    }
	
	/**
	 * Erstellen des Ordner und der Datei, in welche die Daten geschrieben werden sollen
	 */
	@SuppressLint("SimpleDateFormat")
	private void makeLogFolder()
    {
		df = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		String time = df.format(new Date());
		fileName = "BT_Log_" + time + ".txt";
		df = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
		root = new File(Environment.getExternalStorageDirectory() + "/" + this.getString(R.string.app_name));
        try
        {
            if(ov.getlogOn() && !root.exists())
            {
                if(!root.mkdirs())
                    Log.e(getString(R.string.app_name), "Ordner " + root + " konnte nicht erstellt werden");
            }
            if(ov.getlogOn() && (file == null || !file.exists()))
            {
                file = new File(root, fileName);
                FileWriter fw = new FileWriter(file, true);
                BufferedWriter bw = new BufferedWriter(fw);
                if(ov.getUseId())
                    bw.write("Id;MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType"  + "\n");
                else
                    bw.write("MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType" + "\n");
                bw.flush();
                bw.close();
            }
        }
        catch(IOException io)
        {
            Log.e(getString(R.string.app_name), io.getMessage());
        }
    }
	
	/**
	 * NotificationManager erstellen
	 */
	private void initializeNotificationManager()
	{
		noteman = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		ncb 	= new NotificationCompat.Builder(this) 
				.setSmallIcon(R.drawable.appicon)
				.setContentTitle(getString(R.string.app_name))
				.setContentText("Scanner angehalten");
        Intent intent = new Intent(this, MainActivity.class);
        TaskStackBuilder tsb = TaskStackBuilder.create(this)
				.addParentStack(MainActivity.class)
				.addNextIntent(intent);
        PendingIntent pIntent = tsb.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
		ncb.setContentIntent(pIntent);
		noteman.notify(notificationID, ncb.build());
	}
	
	/**
	 * StartButton mit Listener erstellen und Listener starten
	 */
	private void initializeStartButton()
	{
		startScan = (Button)findViewById(R.id.button1);
		/**
		 * OnClick-Listener für den StartButton
		 */
		startScan.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				if(!cl.isScanning())
				{
					startScan.setText("Scanner stoppen");
					cl.setScanning(true);
					gps.mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, lis);
					scanForDevices();
					ncb.setContentText("Scanner läuft");
					noteman.notify(notificationID, ncb.build());
				}
				else
				{
					startScan.setText("Scanner starten");
					cl.setScanning(false);
					btadapter.cancelDiscovery();
					ncb.setContentText("Scanner angehalten");
					noteman.notify(notificationID, ncb.build());
				}
				if(!ov.getShowNew() && !getTitle().equals(getString(R.string.app_name)))
					setTitle(getString(R.string.app_name));
			}
		});
	}

    /**
     * Initialisierung des Listeners für Einstellungsäderungen zur Laufzeit
     */
    @SuppressLint("NewApi")
    private void initializePreferenceChangeListener()
    {
        ov.setVariables();
        myPrefListner = new OnSharedPreferenceChangeListener()
        {
            public void onSharedPreferenceChanged(SharedPreferences sPrefs, String key)
            {
                OptionValues.options changedOption = OptionValues.options.valueOf(key);
                switch (changedOption)
                {
                    case TOGGLE_GPS:
                        ov.setGpsToggle(prefs.getBoolean(key, true));
                        break;
                    case TOGGLE_BT:
                        ov.setBtToggle(prefs.getBoolean(key, true));
                        break;
                    case LOG_ON:
                        ov.setlogOn(prefs.getBoolean(key, true));
                        if(ov.getlogOn())
                        {
                            if(apiLevel >= 16)
                                tv2.setBackground(getResources().getDrawable(R.drawable.textviewbordergreen));
                            tv2.setText("Logging: an");
                        }
                        else
                        {
                            if(apiLevel >= 16)
                                tv2.setBackground(getResources().getDrawable(R.drawable.textviewborderred));
                            tv2.setText("Logging: aus");
                        }
                        break;
                    case TOGGLE_SHOW_NEW:
                        ov.setShowNew(prefs.getBoolean(key, true));
                        break;
                    case GM_VIEW_NEW_ONLY:
                        ov.setMapsShowNewOnly(prefs.getBoolean(key, true));
                        break;
                    case TOGGLE_DISPLAY:
                        ov.setKeepDisplayOn(prefs.getBoolean(key, true));
                        if(ov.isKeepDisplayOn())
                            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        else
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                        break;
                    case USE_DB:
                        ov.setUseDb(prefs.getBoolean(key, false));
                        break;
                    case GM_VIEW_TYPE:
                        ov.setMapsViewType(prefs.getString(key, "1"));
                        break;
                    case SCAN_TYPE:
                        ov.setScanType(prefs.getString(key, "1"));
                        break;
                    case NEW_DEV_SLIDER:
                        ov.setNewDevSlider(prefs.getFloat(key, 0.5f));
                        break;
                    case OLD_DEV_SLIDER:
                        ov.setOldDevSlider(prefs.getFloat(key, 0.25f));
                        break;
                    case SCROLL_TYPE:
                        ov.setScrollType(prefs.getString(key, "1"));
                        break;
                    case TOGGLE_VIBRATION:
                        ov.setToggleVibration(prefs.getBoolean(key, true));
                        break;
                    case SET_TEXT_NEW_DEV_COLOR:
                        ov.setTextNewDevColor(prefs.getInt(key, Color.RED));
                        break;
                    case SET_TEXT_OLD_DEV_COLOR:
                        ov.setTextOldevColor(prefs.getInt(key, Color.BLACK));
                        break;
                    case USE_VENDOR:
                        ov.setUseVendor(prefs.getBoolean(key, false));
                        break;
                    case USE_IGNORE:
                        ov.setUseIgnore(prefs.getBoolean(key, false));
                        if(ov.getUseIgnore())
                        {
                            if(apiLevel >= 16)
                                tv.setBackground(getResources().getDrawable(R.drawable.textviewbordergreen));
                            tv.setText("Ignorierliste: an");
                        }
                        else
                        {
                            if(apiLevel >= 16)
                                tv.setBackground(getResources().getDrawable(R.drawable.textviewborderred));
                            tv.setText("Ignorierliste: aus");
                        }
                        break;
                    case USE_ROW:
                        ov.setUseRow(prefs.getBoolean(key, true));
                        break;
                    case USE_ID:
                        ov.setUseId(prefs.getBoolean(key, false));
                        if(file.exists())
                        {
                            if(ov.getUseId())
                            {
                                try
                                {
                                    BufferedReader in = new BufferedReader(new FileReader(file));
                                    ArrayList<String> content = new ArrayList<String>();
                                    String str;
                                    int start = 0;
                                    while((str=in.readLine()) != null)
                                    {
                                        if(start == 0)
                                        {
                                            content.add("Id;MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType");
                                            start ++;
                                        }
                                        else
                                            content.add(str);
                                    }
                                    in.close();
                                    BufferedWriter out = new BufferedWriter(new FileWriter(file));
                                    for(String line : content)
                                        out.write(line +"\n");
                                    out.close();
                                }
                                catch(IOException ioex)
                                {
                                    Log.e(getString(R.string.app_name), ioex.getMessage());
                                }
                            }
                            else
                            {
                                try
                                {
                                    BufferedReader in = new BufferedReader(new FileReader(file));
                                    ArrayList<String> content = new ArrayList<String>();
                                    String str;
                                    int start = 0;
                                    while((str=in.readLine()) != null)
                                    {
                                        if(start == 0)
                                        {
                                            content.add("MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType");
                                            start ++;
                                        }
                                        else
                                            content.add(str);
                                    }
                                    in.close();
                                    BufferedWriter out = new BufferedWriter(new FileWriter(file));
                                    for(String line : content)
                                        out.write(line +"\n");
                                    out.close();
                                }
                                catch(IOException ioex)
                                {
                                    Log.e(getString(R.string.app_name), ioex.getMessage());
                                }
                            }
                        }
                        break;
                    case ROAD_TYPE:


                        int rt = Integer.parseInt(prefs.getString(key, "0"));
                        switch(rt)
                        {
                            case 0:
                                setrType("A"); //Autobahn
                                break;
                            case 1:
                                setrType("B"); //Bundesstrasse
                                break;
                            case 2:
                                setrType("S"); //Stadt
                                break;
                            case 3:
                                setrType("n.a.");
                                break;
                        }


                        ov.setRoadType(prefs.getString(key, "0"));
                        spinner.setSelection(Integer.parseInt(ov.getRoadtype()));
                        break;
                }
            }
        };
        prefs.registerOnSharedPreferenceChangeListener(myPrefListner);
        if(ov.getUseIgnore())
        {
            if(apiLevel >= 16)
                tv.setBackground(getResources().getDrawable(R.drawable.textviewbordergreen));
            tv.setText("Ignorierliste: an");
        }
        else
        {
            if(apiLevel >= 16)
                tv.setBackground(getResources().getDrawable(R.drawable.textviewborderred));
            tv.setText("Ignorierliste: aus");
        }
        if(ov.getlogOn())
        {
            if(apiLevel >= 16)
                tv2.setBackground(getResources().getDrawable(R.drawable.textviewbordergreen));
            tv2.setText("Logging: an");
        }
        else
        {
            if(apiLevel >= 16)
                tv2.setBackground(getResources().getDrawable(R.drawable.textviewborderred));
            tv2.setText("Logging: aus");
        }
    }

    /**
     * Auswahlbox für Straßentyp (Spinner) mit Listener erstellen und Listener starten
     */
    private void initializeSpinnerListener()
    {
        spinner = (Spinner)findViewById(R.id.spinner);
        spinner.post(new Runnable()
        {
            public void run()
            {
                spinner.setSelection(Integer.parseInt(ov.getRoadtype()));
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener()
        {
            public void onItemSelected(AdapterView<?> arg0, View view, int position, long id)
            {
                int item = spinner.getSelectedItemPosition();
                ov.setRoadType(String.valueOf(item));
                switch(item)
                {
                    case 0:
                        setrType("A"); //Autobahn
                        break;
                    case 1:
                        setrType("B"); //Bundesstrasse
                        break;
                    case 2:
                        setrType("S"); //Stadt
                        break;
                    case 3:
                        setrType("n.a"); //gemischt oder nicht definiert
                        break;
                }
            }
            public void onNothingSelected(AdapterView<?> arg0) { }
        });
    }

    /**
     * Listener für die Erkennung von Änderungen der Einstellungen während die App läuft
     */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key){}
	
	/**
	 * BluetoothReceiver anlegen und registrieren
	 */
	private void initialzeBluetoothReceiver()
	{
		IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
		btr = new BTBondReceiver();
        this.registerReceiver(btr, filter);
        filter = new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
		this.registerReceiver(btr, filter);
		btadapter = BluetoothAdapter.getDefaultAdapter();
	}

	/**
	 *  nach Bluetooth-Geräten suchen
	 */
	private void scanForDevices()
	{
		try
		{
			if(!btadapter.isDiscovering())
				btadapter.startDiscovery();
		}
		catch(Exception ex)
		{
			Log.e(getString(R.string.app_name), ex.getMessage());
		}
	}
	
	/**
	 * Bluetooth aktivieren, falls deaktiviert
	 */
	private void checkBTState()
	{
		if(btadapter == null)
			Toast.makeText(this, "Gerät wird nicht unterstützt", Toast.LENGTH_SHORT).show();
		else
		{
			if(!btadapter.isEnabled())
			{
				Intent enableBT = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				this.startActivityForResult(enableBT, REQUEST_ENABLE_BT);
			}
		}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		if(requestCode == REQUEST_ENABLE_BT)
			checkBTState();
	}
	
	/**
	 * Optionsmenüeintrage hinzufügen
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		menu.add(1, 196, 0, "Ausnahmen löschen");
        menu.add(1, 197, 0, "Textfeld leeren");
		menu.add(1, 198, 0, "Datenbank leeren");
		menu.add(1, 199, 0, "Optionen");
		menu.add(1, 200, 0, "Zeige in GoogleMaps");
		menu.add(1, 201, 0, "Exit");
		menu.add(1, 202, 0, "Über");
		return super.onCreateOptionsMenu(menu);
	}
	
	/**
	 * Optionsmenüeinträge mit Funktion versehen
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item)
    {
		switch (item.getItemId())
		{
            case 196:
                deleteExcludes();
                return true;
            case 197:
				dataList.clear();
				adapter.notifyDataSetChanged();
				return true;
			case 198:
                emptyDatabase();
                return true;
			case 199:
				Intent intent = new Intent(MainActivity.this, AppPreferences.class);  
				startActivity(intent);  
				return true;
			case 200:
				showInGoogleMaps(getAllDeviceLocations());
				return true;
			case 201:
				exitApp();
				return true;
			case 202:
				AboutBox.Show(MainActivity.this);
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

    /**
     * Liste mit den Ausnahmen leeren
     */
    private void deleteExcludes()
    {
        if(excluded != null)
            excluded.clear();
        if(deleteFile(fileNameExclude))
        {
            Toast.makeText(this, "Ausnahmen erfolgreich entfernt!", Toast.LENGTH_SHORT).show();

        }
        else
            Log.e(getString(R.string.app_name), "Datei mit den Geräteausnahmen konnte nicht gelöscht werden!");
    }

    /**
     * Zeit die Geräte aus der ListView in GoogleMaps in einer eigenen Activity
     * @param listToShow Liste mit den Geräteinformationen
     */
    private void showInGoogleMaps(ArrayList<String> listToShow)
    {
        if(isOnline())
        {
            Intent intentG = new Intent(MainActivity.this, googleMain.class);
            intentG.putStringArrayListExtra("coordinates", listToShow);
            intentG.putExtra("viewOption", ov.getMapsViewType());
            startActivity(intentG);
        }
        else
            Toast.makeText(this, "Es ist keine Internetverbindung vorhanden! GoogleMaps kann nicht geladen werden!", Toast.LENGTH_SHORT).show();
    }
	
	/**
	 * Beenden der App mit Nachfragedialog
	 */
	private void exitApp()
	{
		AlertDialog.Builder alertbox = new AlertDialog.Builder(this);
        alertbox.setMessage( this.getString(R.string.app_name) + " beenden?");
        alertbox.setPositiveButton("Ja", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface arg0, int arg1)
            {
                try
                {
                    LocationManager mlocManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
                    if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                    {
                        if (ov.getGpsToggle())
                        {
                            Intent i = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(i);
                        }
                        gps.mlocManager.removeUpdates(lis);
                    }
                    unregisterReceiver(btr);
                    noteman.cancel(notificationID);
                    if (ov.getBtToggle())
                        btadapter.disable();
                    int dispStatus = getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
                    if (dispStatus == 128)
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    System.exit(0);
                }
                catch (Exception ex)
                {
                    Log.e(getString(R.string.app_name), ex.getMessage());
                }
            }
        });
        alertbox.setNegativeButton("Nein", new DialogInterface.OnClickListener() 
        {
        	public void onClick(DialogInterface arg0, int arg1) 
            {
                Toast.makeText(MainActivity.this, "Aktion abgebrochen!", Toast.LENGTH_SHORT).show();
            }
        });
        alertbox.show();
	}

    /**
     * Kontextmenü dür die Elemenete der ListView erstellen
     * @param menu ContextMenu
     * @param v Element, welches mit dem Kontextmenü ausgestattet sein soll
     * @param menuInfo ContextMenuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo)
    {
        if(v.getId() == R.id.listView1)
        {
            menu.setHeaderTitle(getString(R.string.contextMenuTitle));
            String[] menuItems = getResources().getStringArray(R.array.menu);
            for(int i = 0; i < menuItems.length; i ++)
            {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    /**
     * Funktion der das Ausführen einer Aktion bei Klick auf ein Element im Kontextmenü
     * @param item Kontextmenüitem, das angeklickt wurde
     * @return always true
     */
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.menu);
        if(menuItems != null && menuItemIndex >= 0)
        {
            switch(menuItemIndex)
            {
                case 0:
                    if(info != null)
                        showMacVendor(info.position);
                    break;
                case 1:
                    if(info != null)
                        showInGoogleMaps(getSingleDeviceLocation(adapter.getItem(info.position)));
                    break;
                case 2:
                    if(info != null)
                        excludeDevice(info.position);
                    break;
                case 3:
                    if(info != null)
                        deleteFromList(info.position);
                    break;
            }
        }
        else
            Log.e(getString(R.string.app_name), "Kontextmenü konnte nicht erstellt werden");
        return true;
    }

    /**
     * Aufnehmen des Gerätes in eine Textdatei
     * diese enthält alle Geräte, die nicht mehr angezeigt werden sollen
     * @param position Position des angeklickten Elements in der Listview
     */
    private void excludeDevice(int position)
    {
        String[] items = lv.getItemAtPosition(position).toString().split(" ");
        String mac = "";
        for(int i = 0; i < items.length; i ++)
        {
            if(items[i].contains("MAC:"))
            {
                mac = items[i + 1];
                break;
            }
        }
        if(!excluded.contains(mac))
        {
            appendIgnoreFile(fileNameExclude, mac, null);
            excluded.add(mac);
        }
        if(ov.getUseIgnore())
        {
            dataList.remove(position);
            adapter.notifyDataSetChanged();
        }
    }

    /**
     * Gerät aus der Ignorierliste entfernen
     * @param position Position des angeklickten Elements in der Liste
     */
    private void deleteFromList(int position)
    {
        String[] items = lv.getItemAtPosition(position).toString().split(" ");
        String mac = "";
        for(int i = 0; i < items.length; i ++)
        {
            if(items[i].contains("MAC:"))
            {
                mac = items[i + 1];
                break;
            }
        }
        if(excluded != null && excluded.size() > 0)
        {
            for(int i = 0; i < excluded.size(); i++)
            {
                if(excluded.get(i).equals(mac))
                {
                    excluded.remove(i);
                }
            }
           appendIgnoreFile(fileNameExclude, null, excluded);
        }
    }

    /**
     * Anzeigen des Herstellernamens zu einer MAC-Adresse
     * @param position Position des angeklickenten Elements in der ListView
     */
    private void showMacVendor(int position)
    {
        String[] items = lv.getItemAtPosition(position).toString().split(" ");
        String mac = "";
        for(int i = 0; i < items.length; i ++)
        {
            if(items[i].contains("MAC:"))
            {
                mac = items[i + 1];
                break;
            }
        }
        String vendorName = gvi.getVendorName(mac);
        Toast.makeText(MainActivity.this, "Hersteller: " + vendorName, Toast.LENGTH_SHORT).show();
    }

	/**
	 * Datenbank mit den erfassten MACs leeren
	 */
	private void emptyDatabase()
	{
		try
        {
            dbf.deleteTable();
        }
        catch(Exception ex)
        {
            Log.e(getString(R.string.app_name), ex.getMessage());
        }
	}
	
	protected void onDestroy()
	{
		super.onDestroy();
		unregisterReceiver(btr);
		if(ov.getGpsToggle() && gps.mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
			gps.mlocManager.removeUpdates(lis);
        int dispStatus = getWindow().getAttributes().flags & WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
        if(dispStatus == 128)
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		noteman.cancel(notificationID);
		if(dbf.myDB.isOpen())
			dbf.myDB.close();
	}
	
	/**
	 * Prüfen, ob eine Internetverbindung vorhanden ist
	 * @return true, wenn Verbindung vorhanden
	 */
    private boolean isOnline()
	{
	    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
	
	/**
	 * Extrahieren der Geräte-GPS-Koordinaten,
	 * die anschließend als Liste an die GoogleMaps-Activity geschickt werden
	 * @return Liste mit den Kooridnaten
	 */
	private ArrayList<String> getAllDeviceLocations()
	{
		// TODO: Geräte nur anzeigen, wenn sich die Position um einen bestimmten Betrag geändert hat
		if(dataList != null && dataList.size() > 0)
		{
			if(ov.getMapsShowNewOnly())
			{
				ArrayList<String> _tmpList = new ArrayList<String>();
				for(int i = 0; i < adapter.getCount(); i ++)
				{
					String s = adapter.getItem(i);
					if(s.contains("_neu"))
						_tmpList.add(s.substring(0, s.length() - 5));
				}
				return _tmpList;
			}
			else
				return dataList;
		}
		return null;
	}

    /**
     * Extrahieren der Gerätekoordinaten für ein einzelnes Gerät
     * @param devInfo Geräteinformationen, die per Click aus der ListView stammen
     * @return Liste mit den Koordinaten
     */
    private ArrayList<String> getSingleDeviceLocation(String devInfo)
    {
        if(devInfo != null && !devInfo.equals(""))
        {
            ArrayList<String> _tmpList = new ArrayList<String>();
            _tmpList.add(devInfo.substring(0, devInfo.length() - 5));
            return _tmpList;
        }
        return null;
    }
	
	/**
	 * Klasse für den Bluetooth-Empfänger
	 * @author leitzke
	 */
	private class BTBondReceiver extends BroadcastReceiver 
	{
		@SuppressWarnings("ConstantConditions")
        @Override
		public void onReceive(Context context, Intent intent) 
		{
			try
			{
				long[] vibrate = {0, 0};
				String action = intent.getAction();
				if(BluetoothDevice.ACTION_FOUND.equals(action))
				{
					BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
					double[] tmpCoord = cl.getCoordinates();
					String timeStamp = df.format(new Date());
					String bt_data;
                    String deviceAddress;
                    String deviceName;
                    String vendor = "null";
                    String typeOfPhone = "0";
                    if(device != null)
                    {
                        deviceAddress = device.getAddress();
                        deviceName = device.getName();
                    }
                    else
                    {
                        deviceAddress = "n.a.";
                        deviceName = "n.a.";
                    }

                    boolean isIgnored = false;
                    if(ov.getUseIgnore())
                    {
                        if(excluded != null && excluded.size() > 0)
                        {
                            for(String s: excluded)
                            {
                                if(s.equals(deviceAddress))
                                {
                                    isIgnored = true;
                                    break;
                                }

                            }
                        }
                    }
                    if(!isIgnored)
                    {
                        countItems ++;
                        if(ov.getScanType().equals("1"))
                            bt_data = "Nummer: " + countItems + ": " + "MAC: " + deviceAddress + " Lat: " + tmpCoord[0] + " Long: "+ tmpCoord[1] + " Geschw: " + tmpCoord[2]  + " Zeit: " + timeStamp;
                        else if(ov.getScanType().equals("2"))
                            bt_data = "Nummer: " + countItems + ": " + "Name: " + deviceName + " Lat: " + tmpCoord[0] + " Long: "+ tmpCoord[1] + " Geschw: " + tmpCoord[2] + " Zeit: " + timeStamp;
                        else if (ov.getScanType().equals("3"))
                            bt_data = "Nummer: " + countItems + ": " + "MAC: " + deviceAddress + " Name: " + deviceName + " Lat: " + tmpCoord[0] + " Long: "+ tmpCoord[1] + " Geschw: " + tmpCoord[2] + " Zeit: " + timeStamp;
                        else
                            bt_data = "Nummer: " + countItems + ": " + "MAC: " + deviceAddress + " Lat: " + tmpCoord[0] + " Long: "+ tmpCoord[1] + " Geschw: " + tmpCoord[2] + " Zeit: " + timeStamp;
                        if(ov.getUseVendor())
                        {
                            vendor = gvi.getVendorName(deviceAddress);
                            bt_data += " Hersteller: " + vendor;
                            typeOfPhone = gvi.getPhonetype(vendor);
                        }

                        if((ov.getUseDb() && ov.getScanType().equals("1")) || (ov.getUseDb() && ov.getScanType().equals("3")))
                        {
                            if(device != null)
                            {
                                String macAddress  = device.getAddress();
                                if(dbf.isTableExists())
                                {
                                    dbf.openOnly();
                                    String provider = dbf.getDBSingleData(macAddress);
                                    float vib;
                                    if(provider.equals("") || provider.equals("no data"))
                                    {
                                        dbf.insertNewData(macAddress, "");
                                        vib = ov.getNewDevSlider();
                                        bt_data += " _neu";
                                        countNewDev ++;
                                        if(ov.getShowNew())
                                            setTitle(getString(R.string.app_name) + " - " + countNewDev + " neue(s) Gerät(e)");
                                    }
                                    else
                                        vib = ov.getOldDevSlider();
                                    float vib_milli = vib * 1000;
                                    long vib_time = (long)vib_milli;
                                    vibrate = new long[]{0, vib_time};
                                }
                            }
                        }
                        else
                        {
                            float vib;
                            vib = ov.getOldDevSlider();
                            float vib_milli = vib * 1000;
                            long vib_time = (long)vib_milli;
                            vibrate = new long[]{0, vib_time};
                        }
                        if(ov.getUseRow())
                        {
                            if(bt_data.contains("Nummer:"))
                                bt_data = bt_data.replace("Nummer:", "<b>Nummer:</b>");
                            if(bt_data.contains("MAC:"))
                                bt_data = bt_data.replace("MAC:", "<br><b>MAC:</b>");
                            if(bt_data.contains("Hersteller:"))
                                bt_data = bt_data.replace("Hersteller:", "<br><b>Hersteller:</b>");
                            if(bt_data.contains("Name:"))
                                bt_data = bt_data.replace("Name:", "<br><b>Name:</b>");
                            if(bt_data.contains("Lat:"))
                                bt_data = bt_data.replace("Lat:", "<br><b>Lat:</b>");
                            if(bt_data.contains("Long:"))
                                bt_data = bt_data.replace("Long:", "<br><b>Long:</b>");
                            if(bt_data.contains("Geschw:"))
                                bt_data = bt_data.replace("Geschw:", "<br><b>Geschw:</b>");
                            if(bt_data.contains("Zeit:"))
                                bt_data = bt_data.replace("Zeit:", "<br><b>Zeit:</b>");
                        }
                        else
                        {
                            if(bt_data.contains("Nummer:"))
                                bt_data = bt_data.replace("Nummer:", "<b>Nummer:</b>");
                            if(bt_data.contains("MAC:"))
                                bt_data = bt_data.replace("MAC:", "<b>MAC:</b>");
                            if(bt_data.contains("Hersteller:"))
                                bt_data = bt_data.replace("Hersteller:", "<b>Hersteller:</b>");
                            if(bt_data.contains("Name:"))
                                bt_data = bt_data.replace("Name:", "<b>Name:</b>");
                            if(bt_data.contains("Lat:"))
                                bt_data = bt_data.replace("Lat:", "<b>Lat:</b>");
                            if(bt_data.contains("Long:"))
                                bt_data = bt_data.replace("Long:", "<b>Long:</b>");
                            if(bt_data.contains("Geschw:"))
                                bt_data = bt_data.replace("Geschw:", "<b>Geschw:</b>");
                            if(bt_data.contains("Zeit:"))
                                bt_data = bt_data.replace("Zeit:", "<b>Zeit:</b>");
                        }
                        if(ov.getScrollType().equals("1"))
                        {
                            dataList.add(0, bt_data);
                            if(ov.getToggleVibration())
                                mVibrator.vibrate(vibrate, -1);
                        }
                        else if(ov.getScrollType().equals("2"))
                        {
                            dataList.add(bt_data);
                            if(ov.getToggleVibration())
                                mVibrator.vibrate(vibrate, -1);
                            lv.post(new Runnable()
                            {
                                @Override
                                public void run()
                                {
                                    lv.setSelection(adapter.getCount() - 1);
                                }
                            });
                        }
                        else
                            dataList.add(bt_data);
                        adapter.notifyDataSetChanged();
                        if(ov.getlogOn())
                        {
                            FileWriter fw;
                            BufferedWriter bw;
                            if(!root.exists())
                            {
                                if(!root.mkdirs())
                                    Log.e(getString(R.string.app_name), "Ordner " + root + " konnte nicht angelegt werden");
                            }
                            if(file == null || !file.exists())
                            {
                                file = new File(root, fileName);
                                try
                                {
                                    fw = new FileWriter(file, true);
                                    bw = new BufferedWriter(fw);
                                    if(ov.getUseId())
                                        bw.write("ID;MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType"  + "\n");
                                    else
                                        bw.write("MAC;Name;Lat;Long;Geschw;Zeit;Hersteller;PhoneType;RoadType" + "\n");
                                    bw.flush();
                                    bw.close();
                                }
                                catch(IOException ioex)
                                {
                                    Log.e(getString(R.string.app_name), ioex.getMessage());
                                }
                            }
                            try
                            {
                                fw = new FileWriter(file, true);
                                bw = new BufferedWriter(fw);
                                String tmpData;
                                if(ov.getUseId())
                                {
                                    if(ov.getScanType().equals("1"))
                                        tmpData = countItems + ";" + deviceAddress + ";" + "null" + ";" + tmpCoord[0] + ";" + tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else if(ov.getScanType().equals("2"))
                                        tmpData = countItems + ";" + "null" + ";" + deviceName + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else if (ov.getScanType().equals("3"))
                                        tmpData = countItems + ";" + deviceAddress + ";" + deviceName + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else
                                        tmpData = countItems + ";" + deviceAddress + ";" + null + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                }
                                else
                                {
                                    if(ov.getScanType().equals("1"))
                                        tmpData = deviceAddress + ";" + "null" + ";" + tmpCoord[0] + ";" + tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else if(ov.getScanType().equals("2"))
                                        tmpData = "null" + ";" + deviceName + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else if (ov.getScanType().equals("3"))
                                        tmpData = deviceAddress + ";" + deviceName + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                    else
                                        tmpData = deviceAddress + ";" + null + ";" + tmpCoord[0] + ";"+ tmpCoord[1] + ";" + tmpCoord[2] + ";" + timeStamp + ";" + vendor + ";" + typeOfPhone + ";" + getrType();
                                }
                                bw.write(tmpData + "\n");
                                if(bw != null)
                                {
                                    bw.flush();
                                    bw.close();
                                }
                            }
                            catch(Exception ex)
                            {
                                Log.e(getString(R.string.app_name), ex.getMessage());
                            }
                        }
                    }
                }
				else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
				{
					if(cl.isScanning())
						scanForDevices();
				}
			}
			catch(Exception e)
			{
				Log.e(getString(R.string.app_name), e.getMessage());
			}
		}
	}
	
	/**
	 * Klasse für Datenbankzugriffe
	 * @author focmb
	 */
	public class DBFunctions 
	{
		final static String MY_DB_NAME = "mac_organisations";
		final static String MY_DB_TABLE = "macs";
		SQLiteDatabase myDB = null;
		final MainActivity ma;
        private String value_mac;
        private String value_orga;

        public DBFunctions(MainActivity ma)
		{
			this.ma = ma;
		}
		
		/**
		 * Quick and Dirty - Datenbank öffnen
		 */
		public void openOnly()
		{
			try 
	    	{
	    		myDB = openOrCreateDatabase(MY_DB_NAME, MODE_PRIVATE, null);
	    	}
			catch(Exception ex)
			{
				Log.e(getString(R.string.app_name), ex.getMessage());
			}
		}
		
		/**
		 * Datenbak schliessen
		 */
		public void closeOnly()
		{
			if(myDB.isOpen())
				myDB.close();
		}
		
		/**
		 * Datenbank öffnen oder anlegen, falls nicht vorhanden
		 */
		public void onCreateDBAndDBTable()
	    {
	    	try 
	    	{
	    		myDB = openOrCreateDatabase(MY_DB_NAME, MODE_PRIVATE, null);
	    		myDB.execSQL("CREATE TABLE IF NOT EXISTS " + MY_DB_TABLE + " (" + BaseColumns._ID + " integer primary key autoincrement, mac text not null, organisation text not null)");
	        } 
	    	finally
	    	{
	             if (myDB != null)
	                  myDB.close();
	        }
	    }
		
		/**
		 * neue Daten in die Datenbank schreiben
		 * @param value_mac - einzutragende MAC-Adresse als String
		 * @param value_orga - einzutragende Organisation als String
		 */
		public void insertNewData(String value_mac, String value_orga)
		{
            this.value_mac = value_mac;
            this.value_orga = value_orga;
            if(!myDB.isOpen())
			{
				openOnly();
				myDB.execSQL("INSERT INTO " + MY_DB_TABLE + " (mac, organisation) "	+ "VALUES ('" + this.value_mac + "', " + "'" + this.value_orga + "');");
				myDB.close();
				Log.i(getString(R.string.app_name), "Insert new MAC: " + value_mac);
			}
		}
		
		/**
		 * Daten aus der Datenbank lesen
		 * @return String mit der MAC und zugehörigen Organisation
		 */
		@SuppressWarnings("deprecation")
		public String getDBData()
		{
			String result;
			try
			{
			    Cursor c = myDB.rawQuery("SELECT _id, macs || ', ' || organisation as storedMac FROM " + MY_DB_TABLE, null);
			    startManagingCursor(c);   //depricated, but still working
			    result = c.getString(1);
	    		return result;
			}
			finally
			{
			    if (myDB != null)
			     	myDB.close();
			}
		}
		
		/**
		 * * Daten aus der Datenbank lesen
		 * * ÄNDERUNG: Datenbank benutzen, um neue Geräte als solche markeiren zu können
		 * @param value MAC, die geprüft werden soll
		 * @return String mit der MAC (wenn MAC enthalten, dann handelt es sich nicht um eine neue MAC)
		 */
		public String getDBSingleData(String value)
		{
			try
			{
			    if(!myDB.isOpen())
			    	myDB = openOrCreateDatabase(MY_DB_NAME, MODE_PRIVATE, null);
			    Cursor c;
			    c = myDB.rawQuery("SELECT * FROM " + MY_DB_TABLE + " WHERE mac = '"+ value + "'", null);
			    String[] result;
			    int size=c.getCount();
		        if(size > 0)
		        {
		        	result = new String[size];
		        	for(int i = 0; i < size; i ++)
			        {
		        		c.moveToNext();
		        		result[i]=c.getString(c.getColumnIndex("mac"));
			        }
		        	return result[0];
		        }
		        else
		        	return "no data";
		    }
			finally
			{
			    if (myDB != null)
			     	myDB.close();
			}
		}
		
		/**
		 * Tabelle vor einem Update löschen
		 */
		public void deleteTable()
		{
			if(isTableExists())
			{
				AlertDialog.Builder alertbox = new AlertDialog.Builder(ma);
	            alertbox.setMessage("Datenbank wirklich leeren?");
	            alertbox.setPositiveButton("Ja", new DialogInterface.OnClickListener() 
	            {
	                public void onClick(DialogInterface arg0, int arg1) 
	                {
	                	try
	    				{
	                		ProgressDialog dialog = new ProgressDialog(ma);
	                		dialog.setCancelable(true);
	                		dialog.setMessage("Leere...");
	                		dialog.show();
	                		myDB.delete(MY_DB_TABLE, "1", new String[] {});
	    				    myDB.close();
	    				    dialog.cancel();
	    				    Toast.makeText(ma, "Datenbank erfolgreich geleert!", Toast.LENGTH_SHORT).show();
	    				    countNewDev = 0;
	    					if(ov.getShowNew())
	    						setTitle(getString(R.string.app_name) + " - " + countNewDev + " neue(s) Gerät(e)");
	    				}
	    				catch(Exception ex)
	    				{
	    					Log.e(getString(R.string.app_name), ex.getMessage());
	    				}
	                }
	            });
	            alertbox.setNegativeButton("Nein", new DialogInterface.OnClickListener() 
	            {
	            	public void onClick(DialogInterface arg0, int arg1) 
	                {
	                    Toast.makeText(MainActivity.this, "Aktion abgebrochen!", Toast.LENGTH_SHORT).show();
	                }
	            });
	            alertbox.show();
			}
		}
		
		/**
		 * Testen, ob die Tabelle macs existiert
		 * @return 0, wenn die Tabelle nicht existiert
		 */
		boolean isTableExists()
		{
			try
			{
				myDB = openOrCreateDatabase(MY_DB_NAME, MODE_PRIVATE, null);
				if (MY_DB_TABLE == null || myDB == null)
					return false;
				Cursor cursor = myDB.rawQuery("SELECT COUNT(*) FROM sqlite_master WHERE type = ? AND name = ?", new String[] {"table", MY_DB_TABLE});
				if (!cursor.moveToFirst())
					return false;
				int count = cursor.getInt(0);
				cursor.close();
				return count > 0;
			}
			catch(Exception ex)
			{
				Log.e(getString(R.string.app_name), ex.getMessage());
				return false;
			}
		}
	}
	
	/**
	 * Klasse für einen eigenen Arrayadapter zum Anpassen der Schriftgröße in einer Listview;
	 * Einfärben neuer Geräte;
	 * hier kann auch die Hintergrundfarbe eines jeden Eintrags angepasst werden;
	 * @author leitzke
	 */
	class CustomListAdapter extends ArrayAdapter<String> 
	{
	    private final Context mContext;
	    private final int id;
	    private final List <String>items ;

	    public CustomListAdapter(Context context, int textViewResourceId , List<String> list ) 
	    {
	        super(context, textViewResourceId, list);           
	        mContext = context;
	        id = textViewResourceId;
	        items = list ;
	    }

	    @Override
	    public View getView(int position, View v, ViewGroup parent)
	    {
	        View mView = v ;
	        if(mView == null)
	        {
	            LayoutInflater vi = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	            mView = vi.inflate(id, null);
	        }
            TextView text = (TextView) mView.findViewById(R.id.textView);
            if(items.get(position) != null )
	        {
	            if(items.get(position).contains("_neu"))
	            {
	            	String _text = items.get(position);
	            	_text = _text.substring(0, _text.length() - 5);
                    text.setText(Html.fromHtml(_text));
                    text.setTextColor(ov.getTextNewDevColor());
		            text.setBackgroundColor(Color.argb( 255, 255, 255, 255 ) );
	            }
	            else
	            {
	            	text.setTextColor(ov.getTextOldDevColor());
                    text.setText(Html.fromHtml(items.get(position)));
                    text.setBackgroundColor(Color.WHITE);
		        }
	        }
	        return mView;
	    }
	}
}